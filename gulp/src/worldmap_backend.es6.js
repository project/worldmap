((Drupal) => {
  Drupal.behaviors.worldmapBackend = {
    attach(context) {
      const countryListFieldset = context.querySelector('.worldmap__countrylist');

      if (null === countryListFieldset) {
        return false;
      }

      const searchField = countryListFieldset.querySelector('.worldmap__search');
      const countryOptions = Array.from(countryListFieldset.querySelectorAll('.form-wrapper'));
      searchField.addEventListener('input', (event) => {
        const searchTerm = event.target.value.toLowerCase();
        countryOptions.forEach(option => {
          const countryLabel = option.querySelector('label.option').innerText.toLowerCase();

          if (countryLabel.includes(searchTerm)) {
            option.style.display = null;
          }
          else {
            option.style.display = 'none';
          }

        })
      });
    },
  };
})(Drupal);