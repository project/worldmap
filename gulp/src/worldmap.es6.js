import {select} from "d3-selection";
import {zoom, zoomTransform} from "d3-zoom";

((Drupal, select, zoom, zoomTransform) => {
  Drupal.behaviors.worldmap = {
    viewGap: 40,
    duration: 1000,

    attach(context) {
      const mapsAll = Array.from(context.querySelectorAll('.worldmap'));
      mapsAll.forEach(map => {
        this.initMap(map);
      });
    },

    getMapSettings(map, svg) {
      const settings = {};
      const isSingleCountry = this.getAvailableCountries(map).length === 1;
      settings.zoomEnabled = map.getAttribute('data-zoom') === 'on';
      settings.autoZoom = map.getAttribute('data-autozoom') === 'on' && isSingleCountry;
      settings.resetOnClickOutside = map.getAttribute('data-autoreset') === 'on';
      settings.showTooltips = map.getAttribute('data-tooltips') === 'on';
      settings.preventZoomOut = settings.autoZoom === true && map.getAttribute('data-preventzoomout') === 'on';
      settings.displayDropdown = map.querySelector('.worldmap__select') !== null;
      settings.viewBoxInitial = svg.getAttribute('viewBox').toString();

      return settings;
    },

    initMap(map) {
      const svg = map.querySelector('.worldmap__svg');
      const settings = this.getMapSettings(map, svg);

      this.setMapSize(map);
      window.addEventListener('resize', () => {
        this.setMapSize(map);
      });

      if (settings.showTooltips) {
        svg.addEventListener('mousemove', event => {
          this.handleTooltips(event, map);
        });
      }

      const d3countries = select(map).selectAll('path');

      d3countries.on('click', event => {
        this.handleClick(event, map, settings);
      });

      const availableCountries = this.getAvailableCountries(map);

      if (settings.autoZoom && availableCountries.length === 1) {
        const zoomInIcon = map.querySelector('.worldmap__zoom-in-icon');
        const singleCountry = availableCountries.pop();
        if (null !== zoomInIcon) {
          zoomInIcon.addEventListener('click', event => {
            event.stopPropagation();
            this.zoomIn(map, singleCountry, settings);
          });
        }

        this.zoomIn(map, singleCountry, settings);
      }

      // Set change handler for the dropdown.
      if (settings.displayDropdown) {
        const dropdown = map.querySelector('.worldmap__dropdown');
        dropdown.addEventListener('change', event => {
          const countryCode = event.currentTarget.value;
          this.getByDropdown(map, countryCode);
        });
      }
      const zoomOutIcon = map.querySelector('.worldmap__zoom-out-icon');
      if (null !== zoomOutIcon) {
        zoomOutIcon.addEventListener('click', event => {
          event.stopPropagation();
          this.resetMap(map, settings);
        });
      }

      // On click somewhere outside - reset the map.
      if (settings.resetOnClickOutside) {
        map.addEventListener('click', event => {
          event.stopPropagation();
          this.resetMap(map, settings);
        });
      }

    },

    getAvailableCountries(map) {
      const svg = map.querySelector('.worldmap__svg');
      const pathTags = Array.from(svg.querySelectorAll('path'));
      const pathTagsAvailable = [];

      pathTags.forEach(path => {
        if (path.classList.contains('is-selected')) {
          pathTagsAvailable.push(path);
        }
      });

      return pathTagsAvailable;
    },

    setMapSize(map) {
      const svg = map.querySelector('.worldmap__svg');
      const style = window.getComputedStyle(map);
      let widthPx = style.getPropertyValue('width');
      let heightPx = style.getPropertyValue('height');

      if (widthPx < 850) {
        widthPx = 1000;
        heightPx = 500;
      }

      svg.setAttribute('width', parseInt(widthPx, 10));
      svg.setAttribute('height', parseInt(widthPx, 10) / 16 * 7);
    },

    handleClick(event, map, settings) {
      event.stopPropagation();
      const country = event.currentTarget;
      const availableCountries = this.getAvailableCountries(map);

      if (settings.resetOnClickOutside && !country.classList.contains('is-current')) {
        this.resetMap(map, settings);
      }

      if (!country.classList.contains('is-selected')) {
        // If country is not available.
        return false;
      }

      if (!settings.resetOnClickOutside) {
        availableCountries.forEach((path) => {
          path.classList.remove('is-current');
        });
      }

      if (!settings.autoZoom) {
        country.classList.add('is-current');
      }


      if (settings.displayDropdown) {
        const dropdown = map.querySelector('.worldmap__dropdown');
        const option = dropdown.querySelector(`option[value=${country.getAttribute('data-id')}]`);
        option.selected = true;
      }

      if (settings.zoomEnabled) {
        this.zoomIn(map, country);
        setTimeout(() => {
          this.linkCountry(country);
        }, this.duration);
        return;
      }

      this.linkCountry(country);
    },

    resetMap(map, settings) {

      if (settings.autoZoom && settings.preventZoomOut) {
        return;
      }

      if (settings.displayDropdown) {
        const dropdown = map.querySelector('.worldmap__dropdown');
        dropdown.selectedIndex = 0;
      }

      if (settings.zoomEnabled && !map.classList.contains('is-zoomed')) {
        return;
      }
      map.classList.remove('is-zoomed');
      const availableCountries = this.getAvailableCountries(map);
      availableCountries.forEach(country => {
        country.classList.remove('is-current');
        country.classList.remove('is-single');
      });
      const d3svg = select(map).select('.worldmap__svg');
      const d3countries = select(map).selectAll('path');

      const viewBoxParts = settings.viewBoxInitial.split(' ');
      d3svg.transition()
        .duration(this.duration)
        .attr('viewBox', `${viewBoxParts[0]} ${viewBoxParts[1]} ${viewBoxParts[2]} ${viewBoxParts[3]}`);

      d3countries.on('click', null);
      d3countries.on('click', event => {
        this.handleClick(event, map, settings);
      });

    },

    zoomIn(map, path) {
      if (this.getAvailableCountries(map).length === 1) {
        path.classList.add('is-single');
      }
      // zoomTransform;
      let d = path.getAttribute('d');
      let d3countries = select(map).select(`path[d='${d}']`);
      const d3svg = select(map).select('.worldmap__svg');
      map.classList.add('is-zoomed');
      // Add .current class to the country.
      let bBox = d3countries.node().getBBox();
      let x0 = bBox.x - this.viewGap;
      let y0 = bBox.y - this.viewGap;
      let width = bBox.width + 2 * this.viewGap;
      let height = bBox.height + 2 * this.viewGap;

      // Zooming.
      d3svg.transition()
        .duration(this.duration)
        .attr('viewBox', `${x0} ${y0} ${width} ${height}`);
    },

    linkCountry(country) {

      const url = country.getAttribute('data-url');
      if (!url) {
        return;
      }

      window.location.href = url;

    },

    getByDropdown(map, countryCode) {
      const path = map.querySelector(`path[data-id='${countryCode}']`);

      setTimeout(() => {
        path.dispatchEvent(new MouseEvent('click'));
      });
    },

    handleTooltips(event, map) {
      const name = event.target.getAttribute('data-name');
      const tooltip = map.querySelector('.worldmap__tooltip');
      if (!name) {
        // If not on the country.
        tooltip.style.display = 'none';
        tooltip.style.padding = null;
        return;
      }
      // If on country hover.
      tooltip.style.display = null;
      tooltip.style.padding = '10px';

      const x = event.offsetX + 10;
      const y = event.offsetY + 10;
      tooltip.innerText = name;
      tooltip.style.left = x + "px";
      tooltip.style.top = y + "px";
    },

  };
})(Drupal, select, zoom, zoomTransform);