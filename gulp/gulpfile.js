const {
  src, dest, task, parallel, watch, series
} = require('gulp');
const { rollup } = require('gulp-rollup-2');
const { nodeResolve } = require('@rollup/plugin-node-resolve');
const { babel } = require('@rollup/plugin-babel');
const commonjs = require('@rollup/plugin-commonjs');
const stripComments = require('gulp-strip-json-comments');
const removeEmptyLines = require('gulp-remove-empty-lines');
const sass = require("gulp-sass");
const prefix = require("gulp-autoprefixer");
// const debug = require('gulp-debug');
// const using = require('gulp-using');

const styleSrcPattern = './src/*.scss';

const jsDist = '../js';
const styleDist = '../css'

const babelOptions = {
  babelHelpers: 'runtime',
  exclude: 'node_modules/**',
  presets: [['@babel/preset-env', {
    useBuiltIns: false,
    modules: false,
  }]],
  plugins: [
    ['@babel/plugin-transform-runtime', {
      runtimeHelpers: false,
    }],
  ],
};

const babelOptionsBackend = {
  babelHelpers: 'runtime',
  exclude: 'node_modules/**',
  presets: [['@babel/preset-env', {
    useBuiltIns: false,
    modules: 'auto',
  }]],
  plugins: [
    ['@babel/plugin-transform-runtime', {
      runtimeHelpers: true,
    }],
  ],
};

const nodeResolveOptions = {
  moduleDirectories: ['../gulp/node_modules'],
};

// Build JS.
function js() {
  return src('./src/worldmap.es6.js')
    .pipe(rollup({
      output: {
        file: 'worldmap.js',
        name: 'worldmap',
        format: 'iife',
      },
      plugins: [babel(babelOptions), nodeResolve(nodeResolveOptions), commonjs()],
    }))
    .pipe(stripComments())
    .pipe(removeEmptyLines())
    // .pipe(debug({minimal: false}))
    // .pipe(using())
    .pipe(dest(jsDist));
}

// Build JS.
function jsBackend() {
  return src('./src/worldmap_backend.es6.js')
    .pipe(rollup({
      output: {
        file: 'worldmap_backend.js',
        name: 'worldmap_backend',
        format: 'iife',
      },
      plugins: [babel(babelOptionsBackend), nodeResolve(nodeResolveOptions), commonjs()],
    }))
    .pipe(stripComments())
    .pipe(removeEmptyLines())
    // .pipe(debug({minimal: false}))
    // .pipe(using())
    .pipe(dest(jsDist));
}

// Build styles.
function scss() {
  return src(styleSrcPattern)
    .pipe(sass({
      errLogToConsole: false,
      onError: function(err) {
        console.log(err);
      }
    }))
    .pipe(stripComments())
    .pipe(prefix())
    // .pipe(debug({minimal: false}))
    // .pipe(using())
    .pipe(dest(styleDist));
}

task('js', js);
task('jsBackend', jsBackend);
task('styles', scss);

task('default', series('js', 'styles'));

task('watch', () => {
  watch('./src/worldmap.es6.js', parallel('js'));
  watch('./src/worldmap_backend.es6.js', parallel('jsBackend'));
  watch(styleSrcPattern, parallel('styles'));
});
