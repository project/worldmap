<?php

namespace Drupal\worldmap\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Provides a 'World Map' Block.
 *
 * @Block(
 *   id = "worldmap",
 *   admin_label = @Translation("World Map"),
 *   category = @Translation("World Map"),
 * )
 */
class WorldMapBlock extends BlockBase {

  /**
   * Path to country data.
   */
  const COUNTRY_YAML = __DIR__ . '/../../map_data.yml';


  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'countries' => [],
      'links' => [],
      'zoom' => TRUE,
      'display_dropdown' => TRUE,
      'display_zoom_out_icon' => TRUE,
      'auto_zoom' => FALSE,
      'reset_on_click_outside' => FALSE,
      'tooltips' => FALSE,
      'prevent_zoom_out' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $form['country_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
    ];

    $form['country_settings']['zoom'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Zoom-in to country on-click'),
      '#default_value' => !empty($this->configuration['zoom']) ? $this->configuration['zoom'] : NULL,
    ];

    $form['country_settings']['display_dropdown'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display country dropdown list'),
      '#default_value' => !empty($this->configuration['display_dropdown']) ? $this->configuration['display_dropdown'] : NULL,
    ];

    $form['country_settings']['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced Settings'),
    ];

    $form['country_settings']['advanced']['reset_on_click_outside'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reset zoom on click outside'),
      '#default_value' => !empty($this->configuration['reset_on_click_outside']) ? $this->configuration['reset_on_click_outside'] : NULL,
    ];

    $form['country_settings']['advanced']['tooltips'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show country names as tooltips'),
      '#default_value' => !empty($this->configuration['tooltips']) ? $this->configuration['tooltips'] : NULL,
    ];

    $form['country_settings']['advanced']['auto_zoom'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Auto zoom'),
      '#description' => $this->t('Applies only if one country is available'),
      '#default_value' => !empty($this->configuration['auto_zoom']) ? $this->configuration['auto_zoom'] : NULL,
    ];

    $if_auto_zoom_checked = [
      ':input[name$="[country_settings][advanced][auto_zoom]"]' => ['checked' => TRUE],
    ];

    $form['country_settings']['advanced']['prevent_zoom_out'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Prevent zooming-out'),
      '#description' => $this->t('Do not allow to zoom out if auto zoom is enabled'),
      '#default_value' => !empty($this->configuration['prevent_zoom_out']) ? $this->configuration['prevent_zoom_out'] : NULL,
      '#states' => [
        'enabled' => $if_auto_zoom_checked,
        'checked' => $if_auto_zoom_checked,
      ],
    ];

    $country_options = $this->getCountryOptions();
    $form['list'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Available countries'),
      '#attributes' => [
        'class' => ['worldmap__countrylist'],
      ],
    ];

    $form['list']['search'] = [
      '#type' => 'textfield',
      '#title' => FALSE,
      '#size' => 24,
      '#placeholder' => $this->t('Search country...'),
      '#attributes' => [
        'class' => ['worldmap__search'],
      ],
    ];

    foreach ($country_options as $code => $country) {
      $form['list'][$code]['container'] = [
        '#type' => 'container',
        '#title' => FALSE,
      ];
      $form['list'][$code]['container']['country'] = [
        '#type' => 'checkbox',
        '#title' => $country,
        '#attributes' => [
          'class' => ['worldmap__checkbox'],
        ],
        '#default_value' => !empty($this->configuration['countries'][$code]) ? $this->configuration['countries'][$code] : NULL,
      ];

      $form['list'][$code]['container']['link'] = [
        '#type' => 'textfield',
        '#size' => 42,
        '#maxlength' => 128,
        '#placeholder' => 'https://www.example.com/ OR /path/to/internal-page',
        '#default_value' => !empty($this->configuration['links'][$code]) ? $this->configuration['links'][$code] : NULL,
        '#attributes' => [
          'class' => ['worldmap__linkfield'],
        ],
      ];
    }

    $form['#attached']['library'][] = 'worldmap/backend';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    foreach ($form_state->getValue('list') as $country_code => $value) {
      if (empty($value['container'])) {
        continue;
      }
      $this->configuration['countries'][$country_code] = (bool) $value['container']['country'];
      $this->configuration['links'][$country_code] = !empty($value['container']['link']) ? $value['container']['link'] : NULL;
    }
    $this->configuration['zoom'] = (bool) $form_state->getValue(['country_settings', 'zoom']);
    $this->configuration['display_dropdown'] = (bool) $form_state->getValue(['country_settings', 'display_dropdown']);
    $this->configuration['auto_zoom'] = count($this->getAvailableCountries()) === 1 && $form_state->getValue([
        'country_settings',
        'advanced',
        'auto_zoom'
      ]);
    $this->configuration['reset_on_click_outside'] = (bool) $form_state->getValue(['country_settings', 'advanced', 'reset_on_click_outside']);
    $this->configuration['tooltips'] = (bool) $form_state->getValue(['country_settings', 'advanced', 'tooltips']);
    $this->configuration['prevent_zoom_out'] = $this->configuration['auto_zoom'] && $form_state->getValue([
        'country_settings',
        'advanced',
        'prevent_zoom_out'
      ]);
  }


  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $map_data = $this->getMapData();
    return [
      '#theme' => 'worldmap',
      '#config' => $config,
      '#map_data' => $map_data,
      '#module_url' => \Drupal::service('file_url_generator')->generateAbsoluteString(\Drupal::service('extension.list.module')->getPath('worldmap')),
      '#plugin_id' => $this->getPluginId(),
      '#attached' => [
        'library' => [
          'worldmap/frontend',
        ],
      ],
    ];
  }

  /**
   * Parse yaml file.
   *
   * @return array
   */
  protected function parseYamlData() {
    return Yaml::parseFile(static::COUNTRY_YAML);
  }

  /**
   * Get country options.
   *
   * @return array
   */
  protected function getCountryOptions() {
    $options = [];
    $country_list = [];
    \Drupal::moduleHandler()->invokeAll('worldmap_countries', [&$country_list]);
    foreach ($country_list as $code => $country) {
      $options[$code] = $country;
    }
    uasort($options, function ($a, $b) {
      return strcmp($a, $b);
    });
    return $options;
  }

  /**
   * Get map data.
   *
   * @return array
   */
  protected function getMapData() {
    $data = $this->parseYamlData();
    $country_list = [];

    \Drupal::moduleHandler()->invokeAll('worldmap_countries', [&$country_list]);

    foreach ($data as $code => &$country) {
      $country_name_tmp = $country['name'];
      $country['name'] = !empty($country_list[$code]) ? $country_list[$code] : $country_name_tmp;
      $country['selected'] = FALSE;

      if (!empty($this->getConfiguration()['countries'][$code])) {
        $country['selected'] = TRUE;
      }

      $links = $this->getConfiguration()['links'];

      if (!empty($links[$code])) {
        $country['link'] = $links[$code];
      }
    }

    uasort($data, function ($a, $b) {
      return strcmp($a['name'], $b['name']);
    });

    return $data;
  }

  /**
   * Get available countries.
   *
   * @return array
   */
  protected function getAvailableCountries() {
    return array_filter($this->configuration['countries'], function($elem) {
      return (bool)$elem;
    });
  }

}
