(function () {
  'use strict';
  (function (Drupal) {
    Drupal.behaviors.worldmapBackend = {
      attach: function attach(context) {
        var countryListFieldset = context.querySelector('.worldmap__countrylist');
        if (null === countryListFieldset) {
          return false;
        }
        var searchField = countryListFieldset.querySelector('.worldmap__search');
        var countryOptions = Array.from(countryListFieldset.querySelectorAll('.form-wrapper'));
        searchField.addEventListener('input', function (event) {
          var searchTerm = event.target.value.toLowerCase();
          countryOptions.forEach(function (option) {
            var countryLabel = option.querySelector('label.option').innerText.toLowerCase();
            if (countryLabel.includes(searchTerm)) {
              option.style.display = null;
            } else {
              option.style.display = 'none';
            }
          });
        });
      }
    };
  })(Drupal);
})();
