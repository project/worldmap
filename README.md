<pre>
  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘
</pre>

CONTENTS OF THIS FILE
---------------------

* Version
* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

VERSION
-------
Version: 1.0.0-beta1


INTRODUCTION
------------


Adds a configurable SVG-based World map as a block.

**Features:**
- Zoom-in, zoom-out on click
- Auto zoom is available for a single country on the map
- Linked countries

REQUIREMENTS
------------
This module requires JS enabled in browser

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------
**Block configuration:**

- place "World Map" block somewhere on the page
- select available countries (can be zoomed-in)
- provide links for available countries (optional)
- on-click zooming (checkbox)
- single-country auto-zoom (checkbox)
- show country names on-hover (checkbox)
- show zoom-out button in the corner (checkbox)

**Style Customization**
 - Map colors can be changed by overriding CSS variables

MAINTAINERS
-----------

Current maintainers:
* Nikolay Grachev (Granik) - https://www.drupal.org/u/granik / developed by
* Nico Grienauer (Grienauer) - https://www.drupal.org/u/grienauer


by acolono GmbH
---------------

~~we build your websites~~
we build your business

hello@acolono.com

www.acolono.com
www.twitter.com/acolono
www.drupal.org/acolono-gmbh
